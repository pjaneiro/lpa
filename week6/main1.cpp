#include <cstdlib>
#include <cstdio>

#define NMAX 30                                             //Maximum number of places
#define KMAX 22                                             //Maximum number of centroids (not used)

int adj[NMAX][NMAX];                                        //Adjacency matrix, stores the distances between nodes
int cen[NMAX];                                              //Array with centroids, signaled by flag (1 -> is centroid, 0 -> is not centroid)
int n, c;                                                   //Number of places, number of centroids
int result, maxDistance;                                    //Output, maximum distance possible
int i, j, min, max, x, y, d;                                //Indexes, auxiliary variables

int mainLoop(int, int);                                     //Branching part
int check(int);                                             //Bounding part

int main(int argc, char* argv[])
{
    scanf("%d %d",&n,&c);                                   //Read number of places and of centroids
    for(i=0; i<(n*(n-1))/2; i++)
    {
        scanf("%d %d %d", &x, &y, &d);                      //Read distances between places
        adj[x-1][y-1] = d;                                  //Update distance in both ways
        adj[y-1][x-1] = d;
        if(d>maxDistance)                                   //Update maxDistance
        {
            maxDistance = d;
        }
    }
    result = maxDistance;                                   //Starting value

    mainLoop(0,0);                                          //Start main loop, no centroids

    printf("%d\n", result);                                 //Print output

    return 0;
}

int mainLoop(int pos, int count)
{
    max = 0;                                                //Max distance found for current settings

    if(pos >= n)                                            //Limit
    {
        return 0;
    }

    if(count == c)                                          //Test only if we've already got all centroids
    {
        for(i=0; i<n; i++)                                  //Check all nodes
        {
            if(!cen[i])                                     //Test only if the node is not a centroid
            {
                min = maxDistance;                          //The minimum distance for each node is smaller than the maximum possible distance

                for(j=0; j<n; j++)                          //Check all nodes (we're only gonna measure the distances to centroids)
                {
                    if(cen[j])                              //If it's a centroid, measure distance
                    {
                        if(min > adj[i][j])                 //Update minimum distance between nodes and centroids for this settings
                        {
                            min = adj[i][j];
                        }
                    }
                }
                if(max<min)                                 //Update maximum minimum
                {
                    max = min;
                }
            }
        }
        if(result>max)                                      //Update minimum maximum minimum
        {
            result = max;
        }
        return 0;                                           //We can't add centroids, let's move on
    }

    if(check(pos))                                          //If it's feasible to continue, do so
    {
        cen[pos] = 1;                                       //Use node as centroid
        mainLoop(pos+1, count+1);                           //Run loop with increased centroid count
        cen[pos] = 0;                                       //Don't use node as centroid
        mainLoop(pos+1, count);                             //Run loop without increasing centroid count
    }
    return 0;
}

int check(int pos)
{
    max = 0;                                                //Max distance found for current settings

    for(i=0; i<pos; i++)                                    //Check all nodes
    {
        if(!cen[i])                                         //Test only if the node is not a centroid
        {
            min = maxDistance;                              //The minimum distance is smaller than the maximum possible distance
            for(j=0; j<n; j++)                              //Check all nodes
            {
                if(cen[j] || j>=pos)                        //Run only for centroids or for nodes beyond this one
                {
                    if(min>adj[i][j])                       //Update minimum distance between nodes and centroids/unchecked nodes
                    {
                        min = adj[i][j];
                    }
                }
            }
            if(max<min)                                     //Update maximum minimum
            {
                max = min;
            }
        }
    }
    if(max<result)                                          //If it's possible to reach a minimum maximum bigger than the current result, return 1
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
