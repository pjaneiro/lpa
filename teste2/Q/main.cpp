#include <cstdlib>
#include <cstdio>

#define MAX 200
typedef struct
{
    int start;
    int end;
}
activity;
activity list[MAX];
int n,i, limit, instant, result;


int compare(const void* a, const void* b)
{
    activity a1 = *(activity*)a;
    activity b1 = *(activity*)b;
    if(b1.end == a1.end)
    {
        return b1.start-a1.start;
    }
    else
    {
        return a1.end-b1.end;
    }
}

int main(int argc, char* argv[])
{
    scanf("%d", &n);
    for(i=0; i<n; i++)
    {
        scanf("%d", &list[i].start);
        scanf("%d", &list[i].end);
        limit = limit>list[i].end?limit:list[i].end;
    }
    qsort(list, n, sizeof(activity), compare);
    for(i=0; i<n; i++)
    {
        if(instant <= list[i].start)
        {
            instant = list[i].end;
            result++;
        }
    }
    printf("%d\n", result);
    return 0;
}
