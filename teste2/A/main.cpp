#include <cstdlib>
#include <cstdio>
#include <cstring>

#define MAX 100
#define max(a,b) a>b?a:b
int coins[MAX];
int aux[MAX][MAX*MAX];
int n, m, target, sum;
int i, j, mainIndex;
int result;

int main(int argc, char* argv[])
{
    scanf("%d", &n);
    for(mainIndex = 0; mainIndex < n; mainIndex++)
    {
        sum = 0;
        scanf("%d", &m);
        for(i=0; i<m; i++)
        {
            scanf("%d", &coins[i]);
            sum+=coins[i];
        }
        target=sum/2;
        memset(aux, 0, MAX*MAX*MAX);
        result = 0;
        for(i=0; i<m; i++)                                                      //Testamos para todas as moedas que temos
        {
            for(j=1; j<=target; j++)
            {
                if(j>=coins[i] && aux[i][j-coins[i]] + coins[i] > aux[i][j])    //Se a moeda "cabe" na metade, e se a sua soma nos aproxima do alvo
                {
                    aux[i+1][j] = aux[i][j-coins[i]]+coins[i];                  //Adicionamos a moeda
                    result = aux[i+1][j]>result?aux[i+1][j]:result;             //Atualizamos o resultado final
                }
                else
                {
                    aux[i+1][j] = aux[i][j];
                }
            }
        }
        printf("%d\n", sum-(result*2));
    }
    return 0;
}

