#include <cstdlib>
#include <cstdio>

#define NMAX 51
#define MMAX 16

int adj [NMAX][MMAX];
int warehouses[NMAX];
int costs[NMAX];
int n, m;
int result, maxCost;
int i, j, min, max, x, y, d;

int mainLoop(int,int);
int check(int);

int main(int argc, char* argv[])
{
    scanf("%d %d", &n, &m);
    for(i=0; i<n; i++)
    {
        scanf("%d", &costs[i]);
        for(j=0; j<m; j++)
        {
            scanf("%d", &adj[i][j]);
            if(costs[i] + adj[i][j] > maxCost)
            {
                maxCost = costs[i]+adj[i][j];
            }
        }
    }
    result = maxCost;
    mainLoop(0,0);
    printf("%d\n", result);
    return 0;
}

int mainLoop(int pos, int count)
{
    max = 0;
    if(pos>=n)
    {
        return 0;
    }
    for(i=0; i<n; i++)
    {
        if(!warehouses[i])
        {
            min = maxCost;
            for(j=0; j<m; j++)
            {
                if(min > (costs[i]+adj[i][j]))
                {
                    min = costs[i]+adj[i][j];
                }
            }
            if(max<min)
            {
                max = min;
            }
        }
    }
    if(result>max)
    {
        result = max;
    }
    if(check(pos))
    {
        warehouses[i] = 1;
        mainLoop(pos+1, count+1);
        warehouses[i] = 0;
        mainLoop(pos+1, count);
    }
    return 0;
}

int check(int pos)
{
    max = 0;
    for(i=0; i<pos; i++)
    {
        if(!warehouses[i])
        {
            min = maxCost;
            for(j=0; j<n; j++)
            {
                if(warehouses[j] || j>=pos)
                {
                    if(min>costs[i]+adj[i][j])
                    {
                        min = costs[i]+adj[i][j];
                    }
                }
            }
            if(max<min)
            {
                max = min;
            }
        }
    }
    if(max<result)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

