#include <cstdlib>
#include <cstdio>

#define MAX 100

typedef struct
{
    int importance;
    int weight;
    int volume;
} item;
item list[MAX];

int n, mW, mV;
int mainIndex, i, j;
int tests;
int result;

void mainLoop(int, int, int, int);
int check(int, int, int);

int main(int argc, char* argv[])
{
    scanf("%d", &tests);
    for(mainIndex = 0; mainIndex < tests; mainIndex++)
    {
        scanf("%d %d %d", &n, &mW, &mV);
        for(i=0; i<n; i++)
        {
            scanf("%d %d %d", &list[i].importance, &list[i].weight, &list[i].volume);
        }
        mainLoop(0,0,0,0);
        printf("%d\n", result);
    }
    return 0;
}

void mainLoop(int pos, int importance, int weight, int volume)
{
    if(importance>result && weight<=mW && volume<=mV)
    {
        result = importance;
    }
    if(pos==n)
    {
        return;
    }
    if(weight+list[pos].weight<=mW && volume+list[pos].volume<=mV)
    {
        mainLoop(check(pos+1, weight+list[pos].weight, volume+list[pos].volume), importance+list[pos].importance, weight+list[pos].weight, volume+list[pos].volume);
        mainLoop(check(pos+1, weight,volume), importance, weight, volume);
    }
    else if(weight<=mW && volume<=mV)
    {
        mainLoop(check(pos+1, weight, volume), importance, weight, volume);
    }
}

int check(int pos, int weight, int volume)
{
    for(i=pos; i<n; i++)
    {
        if(weight+list[i].weight<=mW && volume+list[i].volume<=mV)
        {
            return i;
        }
    }
    return n;
}

