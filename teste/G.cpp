#include <cstdlib>
#include <cstdio>

#define R 100
#define C 100
#define FALSE 0
#define TRUE 1

char matrix[R][C];
int sequence[R*C];
int r, c;
int i, j;
char found;
int index;

char mainloop(int, int);

int main(int argc, char* argv[])
{
	while(scanf("%d %d", &r, &c) != EOF)
	{
		for(i=0; i<r; i++)
		{
			scanf("%s", matrix[i]);
		}
		found = FALSE;
		for(i=0; i<c; i++)
		{
			if(mainloop(0, i) == TRUE)
			{
				printf("(%d, %d)", 1, sequence[index-1]);
				index = index-2;
				for(j=1; j<r && index >=0; j++)
				{
					printf(",(%d, %d)", j+1, sequence[index]);
					index--;
				}
				printf("\n");
				found = TRUE;
				break;
			}
		}
		if(!found)
		{
			printf("No Path!\n");
		}
	}
	return 0;
}

char mainloop(int level, int pos)
{
	if(pos<0 || pos>=c || level>=r || matrix[level][pos]=='#')
	{
		return FALSE;
	}
	if(level==r-1 && matrix[level][pos]!='#')
	{
		sequence[index++] = pos+1;
		matrix[level][pos]='#';
		return TRUE;
	}
	switch(matrix[level][pos])
	{
		case '|':
			matrix[level][pos]='#';
			if(mainloop(level+1, pos-1)==TRUE || mainloop(level+1, pos)==TRUE || mainloop(level+1, pos+1)==TRUE)
			{
				sequence[index++] = pos+1;
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		case '/':
			matrix[level][pos]='#';
			if(mainloop(level+1, pos-1)==TRUE || mainloop(level+1, pos)==TRUE || mainloop(level, pos-1)==TRUE)
			{
				sequence[index++] = pos+1;
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		case '\\':
			matrix[level][pos]='#';
			if(mainloop(level+1, pos)==TRUE || mainloop(level+1, pos+1)==TRUE || mainloop(level, pos+1)==TRUE)
			{
				sequence[index++] = pos+1;
				return TRUE;
			}
			else
			{
				return FALSE;
			}
	}
	return FALSE;
}
