#include <cstdlib>
#include <cstdio>

#define D1 0																		//Values for rotations
#define D2 1
#define L1 2
#define L2 3
#define R1 4
#define R2 5
#define U1 6
#define U2 7

#define MAX 5																		//Constraint
#define TARGET 0x01010101															//Target, all 1's

#define TRUE 1																		//Boolean values
#define FALSE 0

static const char strings[8][3] = {"D1","D2","L1","L2","R1","R2","U1","U2"};		//Final strings
static const int reverse[9] = {U1, U2, R1, R2, L1, L2, D1, D2, 8};

static const int mat[25][4] = {	{  0,  0,  0,  0},									//This matrix defines how all rotates
								{ 17, 13,  5, 21}, 	/* 1*/
								{ 18, 14,  6, 24}, 	/* 2*/
								{ 19, 15,  7, 23}, 	/* 3*/
								{ 20, 16,  8, 22}, 	/* 4*/
								{  8,  1,  9,  6}, 	/* 5*/
								{  5,  2, 10,  7}, 	/* 6*/
								{  6,  3, 11,  8}, 	/* 7*/
								{  7,  4, 12,  5}, 	/* 8*/
								{ 23,  5, 13, 19}, 	/* 9*/
								{ 22,  6, 14, 20}, 	/*10*/
								{ 21,  7, 15, 17}, 	/*11*/
								{ 24,  8, 16, 18}, 	/*12*/
								{ 14,  9,  1, 16}, 	/*13*/
								{ 15, 10,  2, 13}, 	/*14*/
								{ 16, 11,  3, 14}, 	/*15*/
								{ 13, 12,  4, 15}, 	/*16*/
								{ 11, 20, 18,  1}, 	/*17*/
								{ 12, 17, 19,  2}, 	/*18*/
								{  9, 18, 20,  3}, 	/*19*/
								{ 10, 19, 17,  4}, 	/*20*/
								{  1, 24, 22, 11}, 	/*21*/
								{  4, 21, 23, 10}, 	/*22*/
								{  3, 22, 24,  9}, 	/*23*/
								{  2, 23, 21, 12}}; /*24*/

int first, second;
int minMoves, maxMoves;
int sequence[MAX];
int cenas, i;
int input=0, aux;

inline int turn(int, int);
char mainLoop(int,int,int);

int main(int argc, char* argv[])
{
	scanf("%d",&maxMoves);															//Read maximum number of steps
	minMoves = maxMoves+1;															//Let's try to minimize this number
	for(i=0; i<4; i++)																//Read positions to an integer
	{
		scanf("%d",&aux);
		input<<=8;																	//Make room
		input|= (aux & 0xFF);														//Place
	}
	mainLoop(input, 0, 8);															//Run main loop
	for(i=0; i<minMoves; i++)
	{
		printf("%s ", strings[sequence[i]]);										//Print output
	}
	printf("\n");
	return 0;
}

int turn(int matrix, int rotation)													//This function, given the current positions and a type of rotations, returns the final positions
{
	switch(rotation)																//Calculate changed positions, clear current values, copy new positions
	{
		case D1:
		case U1:
			first = mat[matrix>>24 & 0xFF][rotation/2];								//Calculate new value for the rabbit on the top-left corner. Since we're dealing with integer division, if(n%2)==0, n/2==(n+1)/2
			second = mat[matrix>>8 & 0xFF][rotation/2];								//Calculate new value for the rabbit on the bottom-left corner. Since we're dealing with integer division, if(n%2)==0, n/2==(n+1)/2
			matrix &= 0x00FF00FF;													//Clear the bits for the calculated values
			matrix |= first<<24;													//Update top-left position
			matrix |= second<<8;													//Update bottom-left position
			return matrix;															//Return updated matrix
		case D2:
		case U2:
			first = mat[matrix>>16 & 0xFF][rotation/2];
			second = mat[matrix>>0 & 0xFF][rotation/2];
			matrix &= 0xFF00FF00;
			matrix |= first<<16;
			matrix |= second<<0;
			return matrix;
		case L1:
		case R1:
			first = mat[matrix>>24 & 0xFF][rotation/2];
			second = mat[matrix>>16 & 0xFF][rotation/2];
			matrix &= 0x0000FFFF;
			matrix |= first<<24;
			matrix |= second<<16;
			return matrix;
		case L2:
		case R2:
			first = mat[matrix>>8 & 0xFF][rotation/2];
			second = mat[matrix>>0 & 0xFF][rotation/2];
			matrix &= 0xFFFF0000;
			matrix |= first<<8;
			matrix |= second<<0;
			return matrix;
	}
	return matrix;
}

char mainLoop(int value, int level, int rotation)
{
	if(level + 1  >= minMoves && value!=TARGET)										//Too many movements
	{
		return FALSE;
	}
	if(value == TARGET)																//Sequence found
	{
		minMoves = level;
		return TRUE;
	}
	char toAdd = FALSE;
	for(int j=0; j<8; j++)
	{
		if(j!=reverse[rotation] && mainLoop(turn(value,j), level+1, j) == TRUE)		//There's no point in reversing a movement, so let's try the other options
		{
			sequence[level] = j;													//Found new sequence, let's say which rotation was used
			toAdd = TRUE;
		}
	}
	return toAdd;
}

