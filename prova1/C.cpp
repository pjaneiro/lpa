#include <cstdlib>
#include <cstdio>

#define MAXN 101
#define MAXM 151

int matrix[MAXN][MAXN];
int registos[MAXN];

int n, m, i, j, foo, bar, total;

int main()
{
    scanf("%d %d", &n, &m);
    for(i=0; i<n; i++)
    {
        matrix[i][i] = 1;
    }
    for(i=0; i<m; i++)
    {
        scanf("%d %d", &foo, &bar);
        matrix[foo][bar] = 1;
        matrix[bar][foo] = 1;
    }
    for(i=1; i<=n; i++)
    {
        registos[i] = 1;
        total++;
        for(j=1; j<=n; j++)
        {
            if(matrix[i][j]==0)
            {
                if(registos[j]==1)
                {
                    registos[i]=0;
                    total--;
                    break;
                }
            }
        }
    }
    printf("%d\n",total);
    return 0;
}