#include <cstdlib>
#include <cstdio>
#include <cstring>

#define MAXC 6
#define MAXS 8

char single[MAXS];
int counts[MAXS];
int i, j=-1, s, c, len;
char input, current;
char aux[MAXC];

void mainloop(int count[MAXC], int indexCount, char string[MAXC], int indexString);

int main()
{
    for(i=0; i<MAXS; i++)
    {
        scanf("%c", &input);
        if(input == ' ')
        {
            j++;
            break;
        }
        if(input == current)
        {
            counts[j]++;
        }
        else
        {
            current = input;
            j++;
            single[j] = current;
            counts[j]++;
        }
    }
    scanf("%d", &c);
    mainloop(counts,0,aux,0);
    return 0;
}

void mainloop(int count[MAXC], int indexCount, char string[MAXC], int indexString)
{
    int asdf;
    if(indexString==c)
    {
        printf("%s\n",string);
        return;
    }
    for(asdf = indexCount; asdf<j; asdf++)
    {
        if(count[asdf]>0)
        {
            char newString[MAXC];
            int newCount[MAXC];
            strcpy(newString,string);
            newString[indexString] = single[asdf];
            newString[indexString+1] = 0;
            memcpy(newCount, count, MAXC*sizeof(int));
            newCount[asdf]--;
            mainloop(newCount,asdf,newString,indexString+1);
        }
    }
}
