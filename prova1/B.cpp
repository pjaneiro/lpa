#include <cstdlib>
#include <cstdio>

#define MAXPLAYERS 7
#define MAXPIECES 8

typedef struct piece {
    int num_1;
    int num_2;
    int total;
    int used;
    struct piece * next;
    struct piece * previous;
} Pieces;

int pecas[MAXPLAYERS][MAXPIECES*2][3];
int n, t, i, j;
int maxMoves;
int best;
Pieces * players[MAXPLAYERS];



Pieces * new_piece() {
    Pieces * piece = (Pieces*)calloc(1,sizeof(Pieces));
    return piece;
}

Pieces * findPiece(Pieces * piece, int a, int b) {
    while (piece != NULL) {
        if ((piece->num_1 == a && piece->num_2 == b) || (piece->num_1 == b && piece->num_2 == a) ) {
            return piece;
        }
        else {
            if (piece->next == NULL)
                return NULL;
            else piece = piece->next;
        }
    }
}

Pieces * playPiece(Pieces * piece, int a) {
    while (piece != NULL) {
        if ((piece->num_1 == a || piece->num_2 == a) && piece->total > 0) {
            return piece;
        }
        else {
            if (piece->next == NULL)
                return NULL;
            else piece = piece->next;
        }
    }
}

Pieces * lastPiece(Pieces * piece) {
    while (piece != NULL) {
        if (piece->next == NULL) return piece;
        else piece = piece->next;
    }
}

void addPiece(Pieces * piece) {
    int a, b;
    scanf("%d %d",&a,&b);
    Pieces * found = findPiece(piece, a, b);
    if (found != NULL) {
        found->total++;
    }
    else if (piece->total == 0) {
        piece->num_1 = a;
        piece->num_2 = b;
        piece->total++;
    }
    else {
        piece = lastPiece(piece);
        piece->next = new_piece();
        piece->next->previous = piece;
        piece->next->num_1 = a;
        piece->next->num_2 = b;
        piece->next->total++;

    }
}

void printPieces() {
    int k, l;
    Pieces * piece;
    for (k = 0; k < n; k++) {
        piece = players[k];
        while (piece != NULL) {
            printf("%d-%d %d | ",piece->num_1,piece->num_2,piece->total);
            piece = piece->next;
        }
        printf("\n");
    }
}

int sequence(Pieces * players[], int player, int start, int total_played, int last_played) {
    int next_player = player + 1;
    Pieces * play_piece;
    Pieces * piece;
    int var;
    if (total_played > best) best = total_played;
    if (total_played == 0) {
        piece = players[0];
        i = 1;
        while (i < start*2) {
            if (i % 2 == 0 && i != 0) piece = piece->next;
        }
        if (i % 2 == 0) {
            piece->total--;
            if (next_player == n) next_player = 1;
            var = sequence(players, next_player, start, total_played+1, piece->num_2);
            piece->total++;
            if (var == 0) return 0;
        }
        else {
            piece->total--;
            if (next_player == n) next_player = 1;
            var = sequence(players, next_player, start, total_played+1, piece->num_1);
            piece->total++;
            if (var == 0) return 0;
        }
    }
    else {
        play_piece = playPiece(players[next_player-1],last_played);
        if (play_piece == NULL) return 0;
        else {
            play_piece->total--;
            if (next_player == n) next_player = 1;
            if (piece->num_1 == last_played) var = sequence(players, next_player, start, total_played+1, play_piece->num_2);
            else var =sequence(players, next_player, start, total_played+1, play_piece->num_1 );
            play_piece->total++;
            if (var == 0) return 0;
        }

    }
    return 0;

}

int main()
{
    int a, b;
    scanf("%d %d",&n,&t);
    for (i = 0; i < n; i++) {
        players[i] = new_piece();
        for (j = 0; j < t; j++) {
            addPiece(players[i]);
        }
    }
    printPieces();

    for (a = 0; a < t*2; a++) {
        sequence(players, 1, a, 1, 0);
    }
    printf("%d\n",best);

    return 0;
}