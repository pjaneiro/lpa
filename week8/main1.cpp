#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <climits>

#define NMAX 5000											//Maximum number of banks
#define max(x,y) (x>y?x:y)									//Simple macros for maximum and minimum of two numbers
#define min(x,y) (x<y?x:y)

int adj[NMAX][NMAX];										//Adjacency list
int count[NMAX];											//Number of adjacencies of a node
int money[NMAX];											//Money of each bank, considering the money it owes
int dfs[NMAX];												//Tarjan's index
int low[NMAX];												//Tarjan's lowlink
int stack[NMAX];											//Stack of values
int marked[NMAX];											//Checks if a node has been checked
int nBanks;													//Number of banks
int i, j, k;												//Global indexes for loops
int bank, value, recipient, ind, w, flag, maxMoney;			//Auxiliary variables
int top;													//Stack index

void tarjanAlgorithm(int);
void push(int);
int pop();

int main(int argc, char* argv[])
{
	while(scanf("%d", &nBanks) == 1)						//Read number of banks
	{
		memset(count, 0, nBanks*sizeof(int));				//Reset number of adjacencies
		memset(money, 0, nBanks*sizeof(int));				//Reset money in each bank
		memset(dfs, -1, nBanks*sizeof(int));				//Set to -1 to mark it as non-checked
		memset(low, -1, nBanks*sizeof(int));				//Set to -1 to mark it as non-checked
		memset(marked, 0, nBanks*sizeof(int));				//No node has been marked
		ind = 0;											//Global index is 0
		flag = 0;											//No cluster
		top = 0;											//Reset stack
		maxMoney = INT_MIN;									//Minimum possible value
		for(i=0; i<nBanks; i++)								//Check all banks
		{
			scanf("%d", &bank);								//Read ID
			bank--;
			while(1)
			{
				scanf("%d", &value);						//Read money owed
				if(value==0)								//End of line/End of input
				{
					break;
				}
				else
				{
					scanf("%d", &recipient);				//Read receiver ID
					recipient--;
					money[bank]-=value;						//Update accounts
					money[recipient]+=value;
					adj[bank][count[bank]]=recipient;		//Update adjacency list
					count[bank]++;							//Update number of adjacencies
				}
			}
		}
		for(i=0; i<nBanks; i++)								//For all banks
		{
			if(dfs[i] == -1)								//Index undefined
			{
				tarjanAlgorithm(i);							//Run algorithm
			}
		}
		if(!flag)											//No cluster
		{
			printf("No cluster\n");
		}
		else												//Cluster, print money owed
		{
			printf("%d\n", maxMoney);
		}
	}
	return 0;
}

void tarjanAlgorithm(int v)
{
	dfs[v] = ind;											//Set depth for smallest used index
	low[v] = ind;
	ind++;													//Update index
	int counter = 0;										//No nodes added
	int sum = 0;											//Sum for current cluster, if cluster at all
	push(v);												//Add to stack
	marked[v] = 1;											//Node marked
	int l;													//Index for loop
	for(l=0; l<count[v]; l++)								//For each adjacency
	{
		w = adj[v][l];										//Get node
		if(dfs[w] == -1)									//If undefined
		{
			tarjanAlgorithm(w);								//Unvisited, recurse on it
			low[v] = min(low[v], low[w]);					//Update lowlink
		}
		else if(marked[w])									//Successor on stack
		{
			low[v] = min(low[v], dfs[w]);					//Update lowlink
		}
	}
	if(low[v] == dfs[v])
	{
		sum = 0;
		do
		{
			w = pop();										//Get values from stack
			marked[w] = 0;									//Unmark it
			counter++;										//Counter updated
			sum+=money[w];									//Update local sum
		}
		while(w!=v);
		if(counter>1)										//Cluster existant, check if bigger sum
		{
			flag = 1;										//Cluster existant
			maxMoney = max(maxMoney, sum);					//Update sum
		}
	}
}

void push(int num)
{
	stack[top] = num;
	top++;
}

int pop()
{
	top--;
	return stack[top];
}