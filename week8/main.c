#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

int nBanks;
int listAdj[5000][5001];
int saldOfBank[5000];
int dfs[5000];
int low[5000];
int marcados[5000];
int maxSaldoClust;
int ind;
int haveCluster;


struct stack
{
    int stk[5000];
    int top;
};
typedef struct stack STACK;
STACK s;

void push (int num){
        s.top = s.top + 1;
        s.stk[s.top] = num;
    return;
}

int contains(int num){
    int i;
    for(i=0;i<s.top;i++){
        if(s.stk[i]==num)
            return 1;
    }
    return 0;

}

int pop (){
    int num;
    num = s.stk[s.top];
    s.top = s.top - 1;
    return(num);
}

void leDados(){
    int i,banco, valor, banKre;
    for(i=0;i<nBanks;i++){
        scanf("%d",&banco);
        while(1){
            scanf("%d",&valor);
            if(valor==0)
                break;
            else{
                scanf("%d",&banKre);
                saldOfBank[banco-1]-=valor;
                saldOfBank[banKre-1]+=valor;
                listAdj[banco-1][0]++;
                listAdj[banco-1][listAdj[banco-1][0]]=banKre;
            }

        }
    }
}


void tarjanAlgorithm(v){
    int i;
    int w;
    int aux=0;
    int contador=0;
    dfs[v]=ind;
    low[v]=ind;
    marcados[v]=1;
    ind++;
    push(v);
    printf("V: %d, count %d\n", v, listAdj[v][0]);
    for(i=1;i<=listAdj[v][0];i++){
        w=listAdj[v][i]-1;
        printf("dfs[%d]: %d\n", w, dfs[w]);
        if (dfs[w]==-1){
            tarjanAlgorithm(w);
            low[v]=MIN(low[v],low[w]);
        }
        else if (marcados[w]){
            low[v]=MIN(low[v],low[w]);
        }
    }
    if(low[v]==dfs[v]){
        aux=0;
        do{
            w=pop();
            marcados[w]=0;
            contador++;
            aux+=saldOfBank[w];
        }while(w!=v);
        if(contador>1){
            haveCluster=1;
            maxSaldoClust=MAX(maxSaldoClust,aux);
        }
    }
}

void Tarjan(){
    int v;
    for(v=0;v<nBanks;v++){
        if(dfs[v]==-1)
        {
            tarjanAlgorithm(v);
        }
    }
}





int main(){
    while(scanf("%d",&nBanks)==1){
        memset(listAdj,0,sizeof(listAdj));
        memset(saldOfBank,0,sizeof(saldOfBank));
        memset(dfs,-1,sizeof(dfs));
        memset(low,-1,sizeof(low));
        memset(marcados,0,sizeof(marcados));
        ind=1;
        s.top=-1;
        haveCluster=0;
        maxSaldoClust=-999999999;
        leDados();
        Tarjan();
        if(haveCluster==0)
            printf("No cluster\n");
        else
            printf("%d\n",maxSaldoClust);
    }
    return 0;
}
