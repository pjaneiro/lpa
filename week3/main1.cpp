#include <cstdlib>
#include <cstdio>
#include <cstring>

#define MAX 50
#define FALSE 0
#define TRUE 1

int i, j, l;
int t, n;
int sequence[MAX][MAX];
int count[MAX+1];
int max;

void mainLoop(int, int);

int main(int argc, char* argv[])
{
	scanf("%d", &t);
	for(i=0; i<t; i++)
	{
		max = 0;
		scanf("%d", &n);
		for(j=0; j<n; j++)
		{
			scanf("%d", &(sequence[0][j]));
			count[sequence[0][j]]++;
		}
		mainLoop(0, 0);
		printf("%d\n",max);
	}
	return 0;
}

void mainLoop(int level, int score)
{
	int current = -1, length = 0, pos, k;
	for(k=0; k<n; k++)															//Percorre array de bolas
	{
		if(sequence[level][k] == -1)											//Bola já retirada, passa à posição seguinte
		{
			//continue;
		}
		else if(sequence[level][k] == current)									//Se a bola atual é igual à anterior, adiciona-a à sequência
		{
			length++;
		}
		else if((current == -1 || length<=1) && count[sequence[level][k]] > 1)	//Se encontra uma bola que aparece várias vezes e ainda não está a estudar nenhuma sequência, começa aqui
		{
			current = sequence[level][k];
			length = 1;
		}
		if(length>1 && (k==n-1 || sequence[level][k+1] != current))				//Encontrou uma sequência, há que testá-la
		{
			memcpy(sequence[level+1], sequence[level], n*sizeof(int));
			pos = k;
			int newScore = score+(length*(length+1));
			if(newScore > max)
			{
				max = newScore;
			}
			while(length > 0)
			{
				if(sequence[level][pos] == -1)
				{
					pos--;
				}
				else
				{
					sequence[level+1][pos] = -1;
					pos--;
					length--;
				}
			}
			mainLoop(level+1, newScore);
			current = -1;
			length = 0;
		}
	}
	return;
}
