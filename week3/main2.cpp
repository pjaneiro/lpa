#include <cstdlib>
#include <cstdio>
#include <cstring>

#define MAX 50
#define FALSE 0
#define TRUE 1

int i, j, aux, current;																			//Variáveis meramente auxiliares
int t, n, max;																					//Número de casos de teste, número de bolas em cada caso de teste, pontuação máxima
int sequence[MAX][MAX][2];																		//Sequência de bolas de cada nível de recursividade, em RLE
int seqIndex;																					//Número de conjuntos de bolas inseridas

void mainLoop(int, int, int, int);

int main(int argc, char* argv[])
{
	scanf("%d", &t);																			//Lê número de casos de teste
	for(i=0; i<t; i++)
	{
		current = -1;																			//Começa sem bolas detetadas
		seqIndex = -1;																			//Começa a -1 para o seqIndex++ fazer com que comece a 0
		max = 0;																				//Pontuação máxima começa a 0
		scanf("%d", &n);																		//Lê número de bolas do caso de teste
		for(j=0; j<n; j++)
		{
			scanf("%d", &aux);																	//Lê bola
			if(aux  == current)																	//Se é igual à anterior, aumenta a sua contagem
			{
				sequence[0][seqIndex][1]++;
			}
			else																				//Senão, começa uma nova subsequência
			{
				current = aux;
				seqIndex++;
				sequence[0][seqIndex][0] = aux;
				sequence[0][seqIndex][1] = 1;
			}
		}
		mainLoop(0, 0, 0, seqIndex+1);
		printf("%d\n", max);
	}
	return 0;
}

void mainLoop(int level, int score, int start, int index)
{
	int a, b, c;																				//Índices inerentes à função recursiva
	int eliminadas, newScore, newStart;															//Bolas eliminadas na iteração atual, e número de pontos obtidos com a dita eliminação
	for(a=start; a<index; a++)
	{
		if(sequence[level][a][1] > 1)															//Se se trata de uma subsequência, e não de uma bola isolada, eliminamos
		{
			for(b=0; b<a; b++)
			{
				sequence[level+1][b][0] = sequence[level][b][0];								//Copiamos todas os conjuntos de bolas anteriores
				sequence[level+1][b][1] = sequence[level][b][1];
			}
			if(a>0 && a<index-1 && sequence[level][a-1][0] == sequence[level][a+1][0])			//Se a eliminação provoca a criação de um novo conjunto de bolas adjacentes, juntamo-las
			{
				sequence[level+1][a-1][1] += sequence[level][a+1][1];							//Simplesmente aumentamos o counter do conjunto já inserido
				for(c=a, b=a+2; b<index; b++ && c++)											//Copiamos os conjuntos de bolas seguintes, saltando o que foi agregado
				{
					sequence[level+1][c][0] = sequence[level][b][0];
					sequence[level+1][c][1] = sequence[level][b][1];
				}
				newStart = a-1;																	//Só temos que verificar a partir do novo conjunto criado, os anteriores foram verificados
			}
			else																				//Senão, simplesmente copiamos todos os conjuntos seguintes
			{
				for(c=a, b=a+1; b<index; b++ && c++)
				{
					sequence[level+1][c][0] = sequence[level][b][0];
					sequence[level+1][c][1] = sequence[level][b][1];
				}
				newStart = a;																	//Só temos que testar a partir do conjunto atual, os anteriores já foram verificados
			}
			eliminadas = sequence[level][a][1];													//Número de bolas eliminadas nesta jogada
			newScore = score + (eliminadas * (eliminadas+1));									//Pontuação obtida com esta jogada
			if(newScore > max)																	//Atualizamos a pontuação máxima, se for o caso
			{
				max = newScore;
			}
			mainLoop(level+1, newScore,newStart, c);											//Próximo nível
		}
	}
	return;
}
