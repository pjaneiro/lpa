#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <iostream>

#define MAX 250000

int sums[MAX+MAX];                                                                              //Auxiliary matrix to store sums for each sub-line considered

int r, c, input, total, maxSum;                                                                 //Number of rows, number of columns, and sum of maximum sub-matrix
int i, j, k, top, bottom, matrixIndex, sumsIndex;                                               //Auxiliary indexes
int maxEndingHere, maxSoFar;                                                                    //Auxiliary variables for kadane's algorithm

int main(int argc, char* argv[])
{
    std::ios_base::sync_with_stdio(false);                                                      //Faster input and output
    scanf("%d %d", &r, &c);                                                                     //Get number of rows and columns
    total = r*c;
    if(r<=c)                                                                                    //If the number of columns is bigger than the number of rows, it's faster to do the algorithm with the transposed matrix
    {
        for(i=0; i<total; i++)                                                                  //Fill matrix
        {
            scanf("%d",&input);                                                                 //In a real matrix, we'd use matrix[i][j]
            sums[i+c] = sums[i] + input;
        }
    }
    else                                                                                        //Instead of transposing the array, we'll read each value to its 'transposed position'
    {
        for(i=0; i<r; i++)
        {
            for(j=i; j<total; j+=r)
            {
                scanf("%d",&input);                                                             //In a real matrix, we'd use matrix[j][i]
                sums[j+r] = sums[j] + input;
            }
        }
        i = r;                                                                                  //We also need to swap the number of rows and columns
        r = c;
        c = i;
    }
    for(top=0; top<total; top+=c)                                                               //We'll try every combination of consecutive lines possible, and use kadane's algorithm with each one
    {
        for(bottom=top; bottom<total; bottom+=c)
        {
            maxEndingHere = 0;
            sumsIndex = 0;
            for(k=0; k<c; k++)                                                                  //kadane's algorithm
            {
                maxEndingHere = maxEndingHere + (sums[bottom+k+c]-sums[top+k]);                 //Maximum sum since the beginning/since the last point where the sum was less than 0
                if(maxEndingHere < 0)                                                           //If the current sequence of values makes it pointless for seeking the max value, we'll start over
                {
                    maxEndingHere = 0;
                }
                if(maxSum < maxEndingHere)                                                      //Check if we've reached a new max
                {
                    maxSum = maxEndingHere;
                }
            }
        }
    }
    printf("%d\n", maxSum);
    return 0;
}
