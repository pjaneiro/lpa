#include <cstdlib>
#include <cstdio>
#include <cstring>

#define MAX 250000

int matrix[MAX];																				//"Matrix" with values
int sums[MAX+MAX];																				//Auxiliary matrix to store sums for each sub-line considered

int r, c, total, maxSum;																		//Number of rows, number of columns, and sum of maximum sub-matrix
int i, j, k, left, right, matrixIndex, sumsIndex;												//Auxiliary indexes
int maxEndingHere, maxSoFar;																	//Auxiliary variables for kadane's algorithm

int main(int argc, char* argv[])
{
	scanf("%d %d", &r, &c);																		//Get number of rows and columns
	total = r*c;
	if(r>=c)																					//If the number of columns is bigger than the number of rows, it's faster to do the algorithm with the transposed matrix
	{
		for(i=0; i<total; i++)																	//Fill matrix
		{
			scanf("%d",&matrix[i]);																//In a real matrix, we'd use matrix[i][j]
		}
	}
	else																						//Instead of transposing the array, we'll read each value to its 'transposed position'
	{
		for(i=0; i<r; i++)
		{
			for(j=i; j<total; j+=r)
			{
				scanf("%d",&matrix[j]);															//In a real matrix, we'd use matrix[j][i]
			}
		}
		i = r;																					//We also need to swap the number of rows and columns
		r = c;
		c = i;
	}
	matrixIndex = 0;
	for(i=0; i<r; i++)
	{
		for(j=0; j<c; j++)
		{
			sums[sumsIndex+1] = sums[sumsIndex] + matrix[matrixIndex++];
			sumsIndex++;
		}
		sumsIndex++;
	}
	for(left=0; left<c; left++)																	//We'll try every combination of consecutive columns possible, and use kadane's algorithm with each one
	{
		for(right=left; right<c; right++)
		{
			maxEndingHere = 0;
            sumsIndex = 0;
			for(k=0; k<r; k++)																	//kadane's algorithm
			{
				maxEndingHere = maxEndingHere + (sums[sumsIndex+right+1]-sums[sumsIndex+left]);	//Maximum sum since the beginning/since the last point where the sum was less than 0
				sumsIndex+=c+1;
                if(maxEndingHere < 0)															//If the current sequence of values makes it pointless for seeking the max value, we'll start over
				{
					maxEndingHere = 0;
				}
				if(maxSum < maxEndingHere)														//Check if we've reached a new max for the current array
				{
					maxSum = maxEndingHere;
				}
			}
		}
	}
	printf("%d\n", maxSum);
	return 0;
}
