#include <cstdlib>
#include <cstdio>
#include <cstring>

#define MMAX 70000                              //Maximum number of streets
#define NMAX 600                                //Maximum number of intersections/dead ends

int m, n;                                       //Number of streets and intersections
int adj[NMAX][NMAX];                            //Adjacency matrix
int visited[NMAX];                              //Visited nodes
int path[NMAX];                                 //Path taken
int queue[NMAX];                                //Queue used with the bfs function
int qFront, qEnd;                               //Queue indexes
int max;                                        //Result
int x, y, i, j;                                 //Auxiliary variables

int bfs();                                      //Breadth-first search algorithm

int main(int argc, char* argv[])
{
    scanf("%d %d", &m, &n);                     //Read number of streets and interceptions
    for(i=0; i<m; i++)                          //Read interceptions
    {
        scanf("%d %d", &x, &y);
        adj[x-1][y-1] ++;
        adj[y-1][x-1] ++;
    }
    while(bfs())                                //There's a path leading to the station
    {
        for(i=n-1; i!=0; i=path[i])             //Update paths and adjacency matrix
        {
            j = path[i];
            adj[j][i] --;
            adj[i][j] ++;
        }
        max++;
    }
    printf("%d\n", max);
    return 0;
}

int bfs()
{
    memset(visited, 0, n*sizeof(int));          //Clear visited array
    qFront = 0;                                 //Update queue indexes
    qEnd = 1;
    visited[0] = 1;                             //Start with the hotel
    path[0] = -1;
    while(qFront != qEnd)                       //While the queue isn't empty
    {
        i = queue[qFront];                      //Get current node
        qFront++;                               //Increse queue index
        for(j=0; j<n; j++)
        {
            if(visited[j]==0 && adj[i][j]>0)    //If the street can be visited and we can access it from the current one
            {
                queue[qEnd] = j;                //Add to queue
                qEnd++;
                path[j] = i;                    //Update path
                visited[j] = 1;                 //Update status
            }
        }
    }
    if(visited[n-1] == 1)                       //We reached the station
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

