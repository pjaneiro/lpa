#include <cstdlib>
#include <cstdio>
#include <cstring>

#define NMAX 1000                                                       //Maximum number of neighbourhoods
#define MMAX 6000                                                       //Maximum minimum amount of infected people
int adj[NMAX][NMAX];                                                    //Adjacency list
int s[NMAX];                                                            //Array used to keep track of the order of infected neighbourhoods
int connections[NMAX];                                                  //Number of connections of each neighbourhood
int weight[NMAX];                                                       //Number of people in each neighbourhood
int visited[NMAX];                                                      //Neighbourhoods already visited

int n, m, aux, i, j, target;                                            //Number of neighbourhoods, minimum infected people, connections for current node, indexes, target of an edge
int sSize, sIndex, auxIndex;                                            //Indexes used with s array
int result, infected;                                                   //Number of weeks needed, number of people infected so far
int current, next;                                                      //Current neighbourhood, next neighbourhood

int main(int argc, char* argv[])
{
    while(scanf("%d %d", &n, &m) == 2)                                  //File may contain multiple case tests
    {
        for(i=0; i<n; i++)
        {
            scanf("%d %d", &weight[i], &aux);                           //Read number of people of each neighbourhood, and number of adjacent neighbourhoods
            for(j=0; j<aux; j++)
            {
                scanf("%d", &target);                                   //Adjacent neighbourhood to current one
                if(target>i)                                            //Otherwise, the connection already exists
                {
                    adj[i][connections[i]] = target;                    //Add edge between nodes
                    adj[target][connections[target]] = i;
                    connections[i]++;                                   //Increase number of connections
                    connections[target]++;
                }
            }
            visited[i] = 0;
        }
        visited[0] = 1;                                                 //The neighbourhood has already been visited
        s[0] = 0;                                                       //In the first week, only neighbourhood 0 is infected
        sSize = 1;                                                      //There's 1 neighbourhood in the array
        auxIndex = 1;                                                   //We'll add adjacent neighbourhoods from this position on
        sIndex = 0;                                                     //We'll start checking the array at this position
        result = 0;                                                     //Number of weeks passed is 0
        infected = 0;                                                   //Number of people infected is 0
        while(sSize != sIndex)
        {
            result++;                                                   //Another week passes
            for( ; sIndex < sSize ; sIndex++)
            {
                current = s[sIndex];                                    //Neighbourhood being infected currently
                infected+=weight[current];                              //Update number of infected people
                for(i=0; i<connections[current]; i++)
                {
                    next = adj[current][i];                             //Add each connection to the s array, and update its visited status
                    if(!visited[next])
                    {
                        visited[next] = 1;
                        s[auxIndex++] = next;
                    }
                }
            }
            sSize = auxIndex;                                           //Update total number of added neighbourhoods
            if(infected>=m)                                             //We may have already reached the target
            {
                printf("%d\n", result);
                break;
            }
            else if(sSize == sIndex)                                    //No neighbourhood has been marked for check - we can't reach target
            {
                printf("Safe!\n");
                break;
            }
        }
    }
    return 0;
}

