#include <cstdlib>
#include <cstdio>

#define NMAX 1000                                       //Maximum number of pairs of shoes

int times[NMAX];                                        //Arrival times for the several pairs of shoes
int c, n, t;                                            //Maximum size of each batch, number of pairs of shoes, processing/recovering time
int i;                                                  //General index
int result;                                             //The earliest time to process the last batch of a test case

int compare(const void*, const void*);                  //Comparison function to use within the qsort call

int main(int argc, char* argv[])
{
    while(scanf("%d %d %d", &c, &n, &t) == 3)           //Read the constraints
    {
        result = 0;                                     //Reset the output
        for(i=0; i<n; i++)
        {
            scanf("%d", &times[i]);                     //Read the arrival times
        }
        qsort(times, n, sizeof(int), compare);          //Sort the arrival times
        if(n%c!=0)                                      //If we can't evenly divide the pairs of shoes in complete batches, we must assure that the last batch is full, so the first one may be incomplete
        {
            i = n%c-1;                                  //Number of pairs of shoes in the first batch
            result = times[i];                          //Arrival time of the last pair of shoes for the first batch
            result+=2*t;                                //Increase spent time by the processing time plus the recovery time
            i+=c;                                       //Set the last pair for the second batch
        }
        else
        {
            i = c-1;                                    //If the first batch is complete, set its last pair of shoes
        }
        for( ; i<n; i+=c)                               //Process the following pairs of shoes
        {
            result = result>=times[i]?result:times[i];  //We may have to wait for some pairs of shoes to arrive, or we may have had enough shoes for a complete batch for a while
            result+=2*t;                                //Increase by the processing time plus the recovery time
        }
        result-=t;                                      //We don't have to consider the recovery time for the last batch
        printf("%d\n", result);                         //Print the result
    }
    return 0;
}

int compare (const void* a, const void* b)
{
    return (*(int*)a - *(int*)b);
}
