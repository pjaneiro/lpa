#include <cstdlib>
#include <cstdio>
#include <iostream>

#define NMAX 1000                                       //Maximum number of pairs of shoes

int times[NMAX];                                        //Arrival times for the several pairs of shoes
int c, n, t;                                            //Maximum size of each batch, number of pairs of shoes, processing/recovering time
int i;                                                  //General index
int result;                                             //The earliest time to process the last batch of a test case
int j, a, temp, down, up;                               //Variables used in the sorting function

void sort(int,int);                                     //Comparison function to use within the qsort call
int divide(int,int);

int main(int argc, char* argv[])
{
    std::ios_base::sync_with_stdio(false);
    while(std::cin >> c >> n >> t)
    {
        result = 0;                                     //Reset the output
        for(i=0; i<n; i++)
        {
            std::cin >> times[i];
        }
        sort(0,n-1);                                    //Sort the arrival times
        if(n%c!=0)                                      //If we can't evenly divide the pairs of shoes in complete batches, we must assure that the last batch is full, so the first one may be incomplete
        {
            i = n%c-1;                                  //Number of pairs of shoes in the first batch
        }
        else
        {
            i = c-1;                                    //If the first batch is complete, set its last pair of shoes
        }
        for( ; i<n; i+=c)                               //Process the following pairs of shoes
        {
            result = result>=times[i]?result:times[i];  //We may have to wait for some pairs of shoes to arrive, or we may have had enough shoes for a complete batch for a while
            result+=2*t;                                //Increase by the processing time plus the recovery time
        }
        result-=t;                                      //We don't have to consider the recovery time for the last batch
        std::cout << result << std::endl;
    }
    return 0;
}

void sort(int li, int ls)
{
    if(li<ls)
    {
        a = times[li];
        down = li;
        up = ls;
        while(down<up)
        {
            while(times[down]<=a && down<ls)
            {
                down++;
            }
            while(times[up]>a)
            {
                up--;
            }
            if(down<up)
            {
                temp = times[down];
                times[down] = times[up];
                times[up] = temp;
            }
        }
        times[li] = times[up];
        times[up] = a;
        j = up;
        sort(li,j-1);
        sort(j+1,ls);
    }
}
