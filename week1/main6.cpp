#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <cstring>

#define N_MAX 300000
#define M_MAX 5000

#define FALSE 0
#define TRUE 1

typedef struct
{
	short from, to;
} edge;

short schedule[M_MAX] = {0};
short days[M_MAX] = {0};
edge dependences[N_MAX] = {{0}};

unsigned short m, n, dailyMax;
unsigned short i;
unsigned short foo, bar;
unsigned short daysOverMax=0, maxPerDay=0;

void quicksort(edge*, int, int);
int divide(edge*, int, int);

int main(int argc, char* argv[])
{
	std::ios_base::sync_with_stdio(false);
	
	std::cin >> m >> n >> dailyMax;
	
	for(i=0; i<n; i++)
	{
		std::cin >> dependences[i].from >> dependences[i].to;
	}

	quicksort(dependences, 0, n-1);

	for(i=0; i<n; i++)
	{
		if(schedule[dependences[i].from] > schedule[dependences[i].to])
		{
			schedule[dependences[i].to] = schedule[dependences[i].from] + 1;
		}
		else
		{
			schedule[dependences[i].to]  ++;
		}
	}

	for(i=0; i<m; i++)
	{
		days[schedule[i]] ++;
	}

	for(i=0; i<m; i++)
	{
		if(days[i]>dailyMax)
		{
			daysOverMax++;
		}
		if(days[i]>maxPerDay)
		{
			maxPerDay++;
		}
	}

	std::cout << maxPerDay << " " << daysOverMax << std::endl;

	return 0;
}

void quicksort(edge* data, int li, int ls)
{
	int j;
	if(li<ls)
	{
		j = divide(data, li, ls);
		quicksort(data, li, j-1);
		quicksort(data, j+1, ls);
	}
}

int divide(edge* data, int li, int ls)
{
	edge a, temp;
	int down, up;
	a = *(data+li);
	down = li;
	up = ls;
	while(down < up)
	{
		while(((data+down)->from<a.from || ((data+down)->from==a.from && (data+down)->to<=a.to)) && down<ls)
		{
			down++;
		}
		while((data+up)->from>a.from || ((data+up)->from==a.from && (data+up)->to>a.to))
		{
			up--;
		}
		if(down<up)
		{
			temp = *(data+down);
			*(data+down) = *(data+up);
			*(data+up) = temp;
		}
	}
	*(data+li) = *(data+up);
	*(data+up) = a;
	return up;
}

