#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <cstring>

#define N_MAX 300000
#define M_MAX 5000

#define FALSE 0
#define TRUE 1

#define FROM_NODE 0
#define TO_NODE 1

unsigned short edges[N_MAX][2] = {{0,0}};
short nPrecedences[M_MAX] = {0};
short aux[M_MAX] = {0};

unsigned short m, n, dailyMax, toAssign, edgesAvailable, sSize=0, auxSize=0;
unsigned short i, j;
unsigned short foo, bar;
unsigned short daysOverMax=0, currentDay=0, maxPerDay=0;

int main(int argc, char* argv[])
{
	std::ios_base::sync_with_stdio(false);
	
	std::cin >> m >> n >> dailyMax;
	toAssign = m;
	edgesAvailable = n;

	for(i=0; i<n; i++)
	{
		std::cin >> foo >> bar;
		edges[i][FROM_NODE] = foo;
		edges[i][TO_NODE] = bar;
		nPrecedences[bar]++;
		aux[bar]++;
		sSize++;
	}

	while(toAssign)
	{
		currentDay = 0;
		for(i=0; i<m && sSize>0; i++)
		{
			if(nPrecedences[i] == 0)
			{
				currentDay++;
				toAssign--;
				aux[i]--;
				sSize--;
				for(j=0; j<n && edgesAvailable>0; j++)
				{
					if(edges[j][FROM_NODE] == i)
					{
						aux[edges[j][TO_NODE]]--;
						edgesAvailable--;
						auxSize++;
					}
					
				}
			}
		}
		if(currentDay > maxPerDay)
		{
			maxPerDay = currentDay;
		}
		if(currentDay > dailyMax)
		{
			daysOverMax++;
		}
		memcpy(nPrecedences, aux, m*sizeof(short));
		sSize = auxSize;
		auxSize = 0;
	}

	std::cout << maxPerDay << " " << daysOverMax << std::endl;

	return 0;
}

