#include <cstdlib>
#include <cstdio>
#include <iostream>

#define N_MAX 300000
#define M_MAX 5000

#define FALSE 0
#define TRUE 1

char topics[M_MAX] = {0};											//"booleans" - TRUE means it's already been scheduled
int currentDayTopics[M_MAX] = {0};									//topics studied in the current day, to check if a precedence was scheduled in the day being studied
int precedences[M_MAX][M_MAX] = {{0}};								//stores the precedents for every topic
int precedenceCount[M_MAX] = {0};									//number of precedences of each topic

using namespace std;

int main(int argc, char* argv[])
{
	ios_base::sync_with_stdio(false);								//avoiding sync between iostream objects and stdio streams, I/O speeds increase
	int m, n, dailyMax, initialM;									//m-number of topics, n-number of precedences, dailyMax-the limit of topics handled in a day, initialM-to keep m
	int i, j, k;													//indexes used for loops
	int foo, bar;													//input helpers
	int maxTopicsInDay = 0, daysOverMax = 0, currentDayCount;		//maximum number of topics scheduled for a single day, days where number of topics is above dailyMax, topics in current day
	cin >> m >> n >> dailyMax;										//read number of topics(m), precedences(n) and maximum number of topics for one day(dailyMax)
	initialM = m;													//needed for loop below
	for(i=0; i<n; i++)												//reads each input line
	{
		cin >> foo >> bar;											//I know, foo and bar are lame variable names
		precedences[bar][precedenceCount[bar]] = foo;				//stores precedences in such a way that it's easier to find all precedences for a given topic
		precedenceCount[bar]++;										//keeps track of the number of precedences for a given topic
	}

	while(m>0)														//while there are topics to study
	{
		currentDayCount = 0;										//checks a day at a time
		for(i=0; i<initialM; i++)									//for each topic available
		{
			if(topics[i] == TRUE)									//if already scheduled, ignore
			{
				continue;
			}
			char toAdd = TRUE;										//flag to know if current topic should be added
			for(j=0; j<precedenceCount[i] && toAdd==TRUE; j++)		//for each precedence (assuming we haven't yet ruled the topic out)
			{
				if(topics[precedences[i][j]] == FALSE)				//if precedence has not been met, we can't add the topic yet
				{
					toAdd = FALSE;
					break;
				}
				for(k=0; k<currentDayCount; k++)					//check if precedence has been scheduled for current day
				{
					if(currentDayTopics[k] == precedences[i][j])	//direct comparison
					{
						toAdd = FALSE;
						break;
					}
				}
			}
			if(toAdd == TRUE)										//if the topic is supposed to be scheduled
			{
				topics[i] = TRUE;									//mark it as scheduled
				currentDayTopics[currentDayCount] = i;				//add it to the list of topics scheduled for current day
				currentDayCount++;									//increase counter of topics scheduled for current day
				m--;												//decreases number of topics to schedule
				continue;
			}
		}
		if(currentDayCount>maxTopicsInDay)							//check if maximum number of topics in a single day has been surpased
		{
			maxTopicsInDay = currentDayCount;
		}
		if(currentDayCount>dailyMax)								//check if current day has more topics than Jocas can handle
		{
			daysOverMax++;
		}
	}
	
	cout << maxTopicsInDay << " " << daysOverMax << endl;			//simple output
	return 0;
}

