#include <cstdlib>
#include <cstdio>

using namespace std;

#define MAX 5000

short adjacency[MAX][MAX] = {{0,0}};									//Adjacency matrix for the graph
short precedences[MAX] = {0};											//Number of precendences left for each topic
short s[MAX] = {0};														//S array used with topologic sort

int m, n, dailyMax, toAssign, sSize=0;									//Number of topics, dependences, daily max for Jocas, assignable topics, size of s array
int i, sIndex=0, auxIndex;												//General index, index for s array, auxiliary index for s array
int from, to, current;													//Endpoints for the precedences, current topic
int daysOverMax=0, today=0, maxPerDay=0;								//Days over Joca's daily max, topics studied today, maximum number of topics studied in a single day

int main(int argc, char* argv[])
{
	scanf("%d %d %d", &m, &n, &dailyMax);								//Read number of topics, number of precedences, and number of topics Jocas can study in a single day
	toAssign = m;														//Counter with number of topics left

	for(i=0; i<n; i++)													//Read data (precedences)
	{
		scanf("%d %d", &from, &to);
		adjacency[from][to] = 1;										//Storing data as a table of adjacencies
		precedences[to]++;												//Storing number of precedences for each topic
	}

	for(i=0; i<m; i++)													//Check topics with no precedences
	{
		if(precedences[i] == 0)
		{
			s[sSize++] = i;												//Jocas is ready to study this topic
		}
	}

	auxIndex = sSize;													//There's no point in having multiple arrays, let's just keep 2 indexes and use them accordingly

	while(toAssign)														//While there are topics to study
	{
		if(sSize == sIndex)												//Check for potencial loops
		{
			break;
		}
		today = 0;														//Topics to study today
		for( ; sIndex < sSize; sIndex++)								//Loop through topics Jocas is ready to study
		{
			current = s[sIndex];
			today++;													//Increase counter of topics studied today
			precedences[s[sIndex]] = -1;								//Make sure it won't be picked again
			toAssign--;													//Another one bites the dust
			for(i=0; i<m; i++)											//Loop through dependences
			{
				if(adjacency[current][i] == 1)							//If there's a topic depending on this one
				{
					adjacency[current][i] = 0;							//Clear adjacency
					precedences[i]--;									//One less dependence for the topic
					if(precedences[i] == 0)								//If Jocas is ready to study such topic
					{
						s[auxIndex++] = i;								//Add it to s
					}
				}
			}
		}
		if(today > maxPerDay)											//Check if daily max has been surpassed
		{
			maxPerDay = today;
		}
		if(today > dailyMax)											//Check if it's a desperate day
		{
			daysOverMax++;
		}
		sSize = auxIndex;												//Update index
	}

	printf("%d %d\n", maxPerDay, daysOverMax);

	return 0;
}

