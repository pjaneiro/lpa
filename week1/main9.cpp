#include <cstdlib>
#include <cstdio>

#define MAX 5000

short adjacency[MAX][MAX] = {{0,0}};									//Adjacency matrix for the graph
short s[MAX] = {0};														//S array used with topologic sort
short fromCount[MAX] = {0};												//Number of dependents for each topic
short toCount[MAX] = {0};												//Number of precedences for each topic

int m, n, dailyMax, sSize=0;											//Number of topics, dependences, daily max for Jocas, assignable topics, size of s array
int i, sIndex=0, auxIndex;												//General index, index for s array, auxiliary index for s array
int from, to, current, next;											//Endpoints for the precedences, current topic
int daysOverMax=0, today=0, maxPerDay=0;								//Days over Joca's daily max, topics studied today, maximum number of topics studied in a single day

int main(int argc, char* argv[])
{
	scanf("%d %d %d", &m, &n, &dailyMax);								//Read number of topics, number of precedences, and number of topics Jocas can study in a single day

	for(i=0; i<n; i++)													//Read dependencies
	{
		scanf("%d %d", &from, &to);
		adjacency[from][fromCount[from]++] = to;						//Adds connection to adjacency list, while also increasing number of dependents of current topic
		toCount[to]++;													//Storing number of precedences for each topic
	}

	for(i=0; i<m; i++)													//Check topics with no precedences
	{
		if(toCount[i] == 0)												//If it has no precedences
		{
			s[sSize++] = i;												//Jocas is ready to study this topic
		}
	}

	auxIndex = sSize;													//There's no point in having multiple arrays, let's just keep 2 indexes and use them accordingly

	while(sSize != sIndex)												//While there are topics to study
	{
		today = 0;														//Topics to study today
		for( ; sIndex < sSize; sIndex++)								//Loop through topics Jocas is ready to study
		{
			current = s[sIndex];										//Topic being studied today
			today++;													//Increase counter of topics studied today
			for(i=0; i<fromCount[current]; i++)							//Loop through the dependents of current topic
			{
				next = adjacency[current][i];							//Potencial topic to study the next day
				toCount[next]--;										//Decrease its number of dependences
				if(toCount[next] == 0)									//It is only added if it has no more dependences
				{
					s[auxIndex++] = next;								//Add it to study the next day
				}
			}
		}
		if(today > maxPerDay)											//Check if daily max has been surpassed
		{
			maxPerDay = today;
		}
		if(today > dailyMax)											//Check if it's a desperate day
		{
			daysOverMax++;
		}
		sSize = auxIndex;												//Update index
	}

	printf("%d %d\n", maxPerDay, daysOverMax);

	return 0;
}

