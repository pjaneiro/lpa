#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>

#define N_MAX 300000
#define M_MAX 5000

#define FALSE 0
#define TRUE 1

short schedule[M_MAX] = {0};
short days[M_MAX] = {0};
short dependences[N_MAX][2] = {0};

unsigned short m, n, dailyMax;
unsigned short i;
unsigned short foo, bar;
unsigned short daysOverMax=0, maxPerDay=0;

void quicksort(short[][2], int, int);
int divide(short[][2], int, int);

int main(int argc, char* argv[])
{
	std::ios_base::sync_with_stdio(false);
	
	std::cin >> m >> n >> dailyMax;
	
	for(i=0; i<n; i++)
	{
		std::cin >> dependences[i][0] >> dependences[i][1];
	}

	//std::sort(dependences, dependences+n, order);
	//qsort(dependences, n, sizeof(short[2]), order);
	quicksort(dependences, 0, n-1);

	for(i=0; i<n; i++)
	{
		//std::cin >> foo >> bar;
		if(schedule[dependences[i][0]] > schedule[dependences[i][1]])
		{
			schedule[dependences[i][1]] = schedule[dependences[i][0]] + 1;
		}
		else
		{
			schedule[dependences[i][1]]  ++;
		}
	}

	for(i=0; i<m; i++)
	{
		days[schedule[i]] ++;
	}

	for(i=0; i<m; i++)
	{
		if(days[i]>dailyMax)
		{
			daysOverMax++;
		}
		if(days[i]>maxPerDay)
		{
			maxPerDay++;
		}
	}

	std::cout << maxPerDay << " " << daysOverMax << std::endl;

	return 0;
}

void quicksort(short data[][2], int li, int ls)
{
	int j;
	if(li<ls)
	{
		j = divide(data, li, ls);
		quicksort(data, li, j-1);
		quicksort(data, j+1, ls);
	}
}

int divide(short data [][2], int li, int ls)
{
	short a[2], temp[2];
	int down, up;
	//a[0] = data[li][0];
	//a[1] = data[li][1];
	memmove(a, data[li], 2*sizeof(short));
	down = li;
	up = ls;
	while(down < up)
	{
		while((data[down][0] < a[0] || (data[down][0] == a[0] && data[down][1] < a[1])) && down<ls)
		{
			down++;
		}
		while(data[up][0] > a[0] || (data[up][0] == a[0] && data[up][1] > a[1]))
		{
			up--;
		}
		if(down < up)
		{
			memmove(temp, data[down], 2*sizeof(short));
			memmove(data[down], data[up], 2*sizeof(short));
			memmove(data[up], temp, 2*sizeof(short));
		}
	}
	memmove(data[li], data[up], 2*sizeof(short));
	memmove(data[up], a, 2*sizeof(short));
	return up;
}

