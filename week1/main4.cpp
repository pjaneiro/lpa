#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <cstring>

#define N_MAX 300000
#define M_MAX 5000

#define FALSE 0
#define TRUE 1

short schedule[M_MAX] = {0};
short days[M_MAX] = {0};

unsigned short m, n, dailyMax;
unsigned short i;
unsigned short foo, bar;
unsigned short daysOverMax=0, maxPerDay=0;

int main(int argc, char* argv[])
{
	std::ios_base::sync_with_stdio(false);
	
	std::cin >> m >> n >> dailyMax;
	
	for(i=0; i<n; i++)
	{
		std::cin >> foo >> bar;
		if(schedule[foo] > schedule[bar])
		{
			schedule[bar] = schedule[foo] + 1;
		}
		else
		{
			schedule[bar]  ++;
		}
	}

	for(i=0; i<m; i++)
	{
		days[schedule[i]] ++;
	}

	for(i=0; i<m; i++)
	{
		if(days[i]>dailyMax)
		{
			daysOverMax++;
		}
		if(days[i]>maxPerDay)
		{
			maxPerDay++;
		}
	}

	std::cout << maxPerDay << " " << daysOverMax << std::endl;

	return 0;
}

