#include <cstdlib>
#include <cstdio>
#include <set>                                                          //Elements in sets are stored in a specific order, let's use that in our advantage
#include <algorithm>                                                    //std::sort

typedef struct car                                                      //Struct used to evaluate cars
{
    int x, y, z;
} car;

const bool operator<(const car&, const car&);                           //Necessary to implement a set of cars. It allows them to be directly compared
const bool sorting(const car&, const car&);                             //To use with std::sort. Sorts them in decrasing order of the third criterion

car array[500001];                                                      //Array with all cars
int i, n;                                                               //Index and number of cars
std::set<car> carSet;                                                   //Set with cars, used to sort them according to first criterion
std::set<car>::iterator index;                                          //Iterator to iterate through set
bool flag;                                                              //Flag to check if a car should be inserted

int main(int argc, char* argv[])
{
    scanf("%d", &n);                                                    //Read number of cars
    for(i=0; i<n; i++)
    {
        scanf("%d %d %d", &array[i].x, &array[i].y, &array[i].z);       //Read info of all cars
    }
    std::sort(array, array+n, sorting);                                 //Sort in a decreasing order according to third criterion
    carSet.clear();                                                     //Clear set, just in case
    for(i=0; i<n; i++)
    {
        index = carSet.lower_bound(array[i]);                           //Iterator points to the first element in the set whose x value is equal or bigger than the one of the current element
        flag = false;                                                   //So far, we don't know if it should be inserted
        if(index != carSet.begin())                                     //Decrease index if it's not the beginning of the set
        {
            index--;
        }
        else                                                            //If it's the beginning, no car is better according to first criterion, flag to insert it
        {
            flag = true;
        }
        if(index->y < array[i].y || flag)                               //Check if y value is bigger than the one on the current position of the index
        {
            index = carSet.insert(array[i]).first;                      //Index points to the inserted element
            printf("%d %d %d\n", array[i].x, array[i].y, array[i].z);
            index++;                                                    //Increase index so we can check all cars starting on the following one
            while(index != carSet.end())                                //Check all remaining cars
            {
                if(index->y <= array[i].y)                              //If it's worst according to second criterion, erase it
                {
                    carSet.erase(index++);
                }
                else
                {
                    break;
                }
            }
        }
    }
}

const bool operator<(const car& a, const car& b)
{
    return a.x > b.x;
}

const bool sorting(const car& a, const car& b)
{
    return a.z > b.z;
}
